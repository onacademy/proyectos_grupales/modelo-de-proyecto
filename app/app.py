from datetime import datetime

from flask import Flask, render_template, jsonify, request
from flask.helpers import make_response

from app.model.calculadora import Calculadora

app = Flask(__name__)
calculadora = Calculadora()


@app.route('/')
def index():
    return render_template("index.html", fecha=datetime.now())


# POST /sumar { x: y: }  => 200 { resultado: }
@app.route('/sumar', methods=['POST'])
def sumar():
    datos = request.get_json()
    x_value = datos.get('x')
    y_value = datos.get('y')
    resultado = calculadora.sumar(x_value, y_value)
    cantidad_operaciones = calculadora.cantidad_de_operaciones()
    respuesta = {'resultado': resultado, 'cantidad_operaciones': cantidad_operaciones}
    return make_response(jsonify(respuesta), 200)
