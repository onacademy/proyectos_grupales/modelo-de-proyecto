class Calculadora:

    def __init__(self):
        self.contador_de_operaciones = 0

    def sumar(self, x_value, y_value):
        self.contador_de_operaciones += 1
        return x_value + y_value

    def cantidad_de_operaciones(self):
        return self.contador_de_operaciones
